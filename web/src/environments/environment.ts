// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  employeeApi : "http://127.0.0.1:8000/employees",
  managerApi : "http://127.0.0.1:8000/managers",
  employeeDataApi: "http://127.0.0.1:8000/employeeData",
  connectionApi: "http://127.0.0.1:8000/karoapi/connect",
  notificationApi: "http://127.0.0.1:8000/notifications",
  markasreadApi: "http://127.0.0.1:8000/karoapi/markasread/",
  notificationBoxApi: "http://127.0.0.1:8000/notificationBox",
  fatigueRecordApi: "http://127.0.0.1:8000/fatigueRecords",
  passwordResetApi : "http://127.0.0.1:8000/password_reset/",
  passwordResetConfirmApi: "http://127.0.0.1:8000/password_reset/confirm/",
  passwordResetTokenValidateApi: "http://127.0.0.1:8000/password_reset/validate_token/"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
