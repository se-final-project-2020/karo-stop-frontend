import { Component } from '@angular/core';
import { EmployeeApiService } from './employee-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'web';
  employees = [{list: 'test'}];

  constructor(private api: EmployeeApiService) {
    this.getEmployees
  }
  getEmployees = () => {
    this.api.getAllEmployees().subscribe(
      data => {
        this.employees = data;
      },
      error => {
        console.log(error)
      }
    )
  }
}
