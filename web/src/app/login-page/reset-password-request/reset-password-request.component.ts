import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-reset-password-request',
  templateUrl: './reset-password-request.component.html',
  styleUrls: ['./reset-password-request.component.css']
})
export class ResetPasswordRequestComponent implements OnInit {
 //Error
 error: any;
 //Hide password, might need to remove this for safety concern
 hide:boolean;
 
 authenticating:boolean;
 
 constructor(
   private fb: FormBuilder,
   private router:Router,
   private route: ActivatedRoute,
   private http: HttpClient
 ) { }
 //Indicator if the email has been submitted
 emailSubmitted : boolean = false //Default is false
 //Indicator if the submitted email is valid (aka exists in the backend or else how would you get the mail)
 emailValid     : boolean = false //Default is undefined
 //Email Form
 form = this.fb.group({
  email: ['', [Validators.required, Validators.email]],
 })

 validation_messages = {
  'email': [
    { type: 'required', message: 'Please enter your email'},
    { type: 'pattern', message: 'Email not in a correct format'}
  ]
}

 ngOnInit() {
  
 }

 //Whatever stuff you do after submit lmao pog
 submit() {
   this.requestPasswordLink()
 }

 // Request a link to reset password, which is sent to the submitted email
 requestPasswordLink() {
   const email = this.form.value.email;
   this.error = undefined
   this.http.post(environment.passwordResetApi,
        {
          "email": email
        },
        {observe: 'response'}
   ).subscribe( 
    //Success 
    response => {
        this.emailSubmitted = true
        this.emailValid = true
     },
    //Failure
    error => {
      this.error = error
        console.log(error.error)
        this.emailValid = false
     }
     )
 }

  //Go back to the login page
  goBack() {
    this.router.navigate(['/login']);
  }
 

}
