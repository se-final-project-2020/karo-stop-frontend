import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import * as jwt_decode from "jwt-decode";
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})


export class LoginPageComponent implements OnInit {
  //Error
  error: any;
  //Hide password, might need to remove this for safety concern
  hide:boolean;
  
  authenticating:boolean;
  
  constructor(
    private authService:AuthService,
    private fb: FormBuilder,
    private router:Router,
    private route: ActivatedRoute
  ) { }

  //Login Form
  form = this.fb.group({
   
    username_or_email: [''],
    password: ['']
  
  })

  ngOnInit() {
    //Log out if you reach the login page lmao
    this.authService.logout();
  }

  // Login to either Employee Status Panel Page or Manager Dashboar Page, depending on the User
  login() {
    const value = this.form.value;
    //this.authService.login(value.username, value.password).subscribe(
      this.authService.login(value.username_or_email, value.password).subscribe(
      success => 
      {
        var is_staff = this.authService.getUser().is_staff;
        if (is_staff)
          this.router.navigate(['/manager/dashboard'])
        else
          this.router.navigate(['/employee/dashboard'])
        
      },
      error => this.error = error
    );
  }
  
  // The website should require you to login after time, even when you exit the browser, so probably need to do sth with local storage
  // No better yet, switch to Django Default token (which keeps token in the DB for validation) or session-based authentication
}
