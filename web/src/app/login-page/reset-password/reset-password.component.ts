import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder
    ) {}
  //Password Reset Token
  token;
  //Password Reset Token Validation Link
  tokenValidateApi = "http://127.0.0.1:8000/password_reset/validate_token/"

  unmatchedPassword : boolean
  hide : boolean
  //Indicate if given token is being validated
  validatingToken: boolean = true //Default true
  //Indicate if token is valid
  validToken: boolean = false //Default false
  //Indicate if password is successfully resetted
  isReset: boolean = false //Default false
  //Password Reset Form
  form = this.fb.group({
    //New Password
    newPassword: ['',Validators.compose([Validators.required])],
    //Confirm New Password
    confirmNewPassword: ['',Validators.compose([Validators.required])],
  },{
      validator: this.matchPassword('newPassword', 'confirmNewPassword')
  })
    
  //Match 2 password fields
  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];
  
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }

  //Validation Messages for the form
    validation_messages = {
      'newPassword': [
        { type: 'required', message: 'Please enter your new password'}
      ],
      'confirmNewPassword': [
        { type: 'required', message: 'Please enter your new confirm password'},
        { type: 'mustMatch', message: 'New passwords do not match'}
      ]
    };
  
  //Run on launch
  ngOnInit() {
  //Fetch params from the url
    this.route.queryParams.subscribe((params) => {
        this.token = params['token']
        console.log(this.token)
    });
    //Validate Password Reset Token
    this.validatePasswordResetToken()
    
  }

  //Validate Password Reset Token with the backend server
  validatePasswordResetToken() {
    this.http.post(
      //Check if token is still valid
      this.tokenValidateApi, { "token": this.token }, {observe: 'response'}
        ).subscribe (
          //Successful
          response => {
            //Display form to input new passwords
            this.validToken = true
            this.validatingToken = false
          },
          //Failure
          error => {
             //Show failure and ask to go to login page
            this.validToken = false
            this.validatingToken = false
          }
  
      )
  }

  //Submit new Password and existing token
  submit() {
    this.http.post(
      environment.passwordResetConfirmApi,
      {
        "password": this.form.get('newPassword').value,
        "token"   : this.token
      }
    ).subscribe(
      //Success
      data=> { 
        console.log(data)
        this.isReset = true
      },
      //Failure
      error=> {
        //Show failure and ask to go to login page
        console.log(error.error)
        this.validToken = false
        this.validatingToken = false
      }
      )
    
  }

  //Go back to the login page
  goBack() {
    this.router.navigate(['/login']);
  }
}