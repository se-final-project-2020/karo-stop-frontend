import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../service/backend-service.service';
import EmployeeData from '../entity/employee-data';
import Employee from '../entity/employee';
import BreakRequest from '../entity/break-request';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import NotificationBox from '../entity/notification-box';
import Notification from '../entity/notification';
import { MatSidenav } from '@angular/material/sidenav';
import { interval } from 'rxjs';
import { AuthService } from '../auth/auth.service';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent  {
  
  
  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  
  //Log out from the system, gotta find a way to acess the actual stuff on the page
  logout() {
    this.user = undefined;
    this.account_role_id = undefined
    this.router.navigate(['']);
    this.authService.logout();
    this.sidenav.close()
  }
  
  constructor(
    private breakpointObserver: BreakpointObserver,
    private backendService: BackendService,
    private authService: AuthService,
    private router: Router,
    private http: HttpClient) 
     {}
    //Bind to notification sidenav and check if it is opened
    @ViewChild('sidenav') sidenav: MatSidenav;
    opened: boolean;

    employee:Employee;
    employeeData :any;
    firstName: string;
    lastName: string;
   
    notifications: Notification[] = []
    //Number of unread notifications to display as a badge
    unreadNotification: number = 0;
    //ID for the real user account
    account_role_id

    //Toggle the sidenav for notification box
    toggleSideNav(): boolean {
      this.sidenav.toggle();
      //If we close the notification sidenav, mark all messages as read, and reload the notification
      if (!this.sidenav.opened) {
          for (var i=0; i<this.notifications.length;i++) {
            var notif = this.notifications[i]
            if(!notif.is_read) {
              this.backendService.markNotificationAsRead(notif.id).subscribe( data=> {
                console.log(data)
              })
            }
          }
          this.updateNavigationBar()
      }
      //Return if the sidenav is opened or not
      return this.sidenav.opened
    }

     //Will need to be adjusted to accomodate Manger user
    ngAfterViewInit(): void {
      //If this nav will both be used for manager and employee, might want to do sth about separating the tasks here
      //For Employee
      setInterval(()=> { 
        if (this.isLoggedIn()) {
          //Get User if not yet fetched
          if (this.user == undefined || this.user == null) {
            this.getUser().subscribe( data=> {
              this.user = data
              console.log("Got user data for Nav")
              this.updateNavigationBar()             
            })
          }
          else {
              this.updateNavigationBar()
          }
        } },2000);
  }

        updateNavigationBar(): void {
            this.UpdateDataForNav()
        }



        private UpdateDataForNav() {
            // Update Notification Box for Employee
            if(!this.user.is_staff) {
              //If account_role_id has not been fetched yet, fetch it first
              if(this.account_role_id == undefined) {
                this.backendService.getEmployeeId(this.user.id).subscribe( data=> {
                    this.account_role_id = data.employee_id
                    this.updateEmployeeData()
                  })
              }
              else {
                    this.updateEmployeeData()
              }
            }
            // Update Notification Box for Manager
            else {
              //Update Manager Data
              //If account_role_id has not been fetched yet, fetch it first
              if(this.account_role_id == undefined) {
                this.backendService.getManagerId(this.user.id).subscribe( data=> {
                    this.account_role_id = data.manager_id
                    this.updateManagerData()
                  })
              }
              else {
                    this.updateManagerData()
              }
            }
         }
        
        //Update Employee Data using Employee ID
        private updateEmployeeData() {
            this.backendService.getEmployee(this.account_role_id).subscribe( data=> {
              this.employeeData = data
              this.updateNotification(data)
            });   
        }
        breakRequests : BreakRequest[]
        //Update Manger Data using Employee ID
        private updateManagerData() {
            this.backendService.getManager(this.account_role_id).subscribe( manager=> {
              //this.updateNotification(manager)
              this.updateNotificationAndBreakRequest(manager)
              //Get breakRequest
              
            });      
        }

        getBreakRequests(): Observable<BreakRequest[]> {
          return this.http.get<BreakRequest[]>("http://127.0.0.1:8000/breakRequests/")
        }
        
       
        

        //Update Notification for the account
        private updateNotification(data) {
              this.notifications = data.notifications
              //Get the amount of unread notifications
              this.unreadNotification = this.notifications.reverse().filter(i=>i.is_read==false).length;
                  
        }

        //Update Notification for the account
        private updateNotificationAndBreakRequest(data) {

                this.getBreakRequests().subscribe ( breakRequests=> {
                  this.breakRequests = breakRequests
                  this.notifications = data.notifications
                  //Get the amount of unread notifications
                  this.unreadNotification = this.notifications.reverse().filter(i=>i.is_read==false).length;
                  this.unreadNotification += this.breakRequests.filter(i => (!i.is_accepted && !i.is_denied)).length
                })
                
            }
        
         //Response to the break request made by the employee
         answerBreakRequest(breakRequestId, isAccepted) {
          if (isAccepted) {
            this.breakRequests.filter(i => i.id == breakRequestId)[0].is_accepted = true
            return this.http.patch("http://127.0.0.1:8000/breakRequests/"+breakRequestId+"/", { "is_accepted":true}).subscribe()
          }
          else {
            this.breakRequests.filter(i => i.id == breakRequestId)[0].is_accepted = true
            return this.http.patch("http://127.0.0.1:8000/breakRequests/"+breakRequestId+"/", { "is_denied":true}).subscribe()
          }
        }
        

    user : any
    getUser() {
      return this.http.get("http://127.0.0.1:8000/users/"+this.authService.getUser().user_id)
    }
    
    isLoggedIn() {
      return  this.authService.isLoggedIn()
    }
    
    ngOnInit(): void {

    }
    

}
