import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { EmployeeOverviewDashboardComponent } from './dashboard/employee/employee-overview-dashboard/employee-overview-dashboard.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthGuard } from './auth/auth.service';
import { ManagerOverviewDashboardComponent } from './dashboard/manager/manager-overview-dashboard/manager-overview-dashboard.component';
import { ResetPasswordComponent } from './login-page/reset-password/reset-password.component';
import { ResetPasswordRequestComponent } from './login-page/reset-password-request/reset-password-request.component';
import { ManagerEmployeelistComponent } from './manager-employeelist/manager-employeelist.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';




const systemRoutes: Routes = [
  //Employee Status Panel Page, required to be authenticated as Employee
  {
    path: 'employee/dashboard',
    component: EmployeeOverviewDashboardComponent,
    canActivate: [AuthGuard] ,
    data: { for_staff: false }
  },
  //Manager Dashboard Page, required to be authenticated as Manager
  {
    path: 'manager/dashboard',
    component: ManagerOverviewDashboardComponent,
    canActivate: [AuthGuard] ,
    data: { for_staff: true } 
  },
  //Manager's Employeelist Page, required to be authenticated as Manager
  {
    path: 'manager/employeelist',
    component: ManagerEmployeelistComponent,
    canActivate: [AuthGuard],
    data: { for_staff: true }
  },
  //Profile Page, can be either Employee or Manager
  {
    path: 'manager/profile/:id',
    component: ProfilePageComponent,
    canActivate: [AuthGuard],
    data: { for_staff: true }
  },
  //Login Page
  { 
    path: 'login',
    component: LoginPageComponent    
  },
  //Reset Password Page
  { 
    path: 'reset_password',
    component: ResetPasswordComponent  
  },
  //Reset Password Request Page
  { 
    path: 'reset_password_request',
    component: ResetPasswordRequestComponent  
  },
  //Default page, might need to chagnge stuff
  { path: '', redirectTo: 'login', pathMatch: 'full' }
  

];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
