import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import Employee from '../entity/employee';
import EmployeeData from '../entity/employee-data';
import { HttpClient, HttpResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { ; }

  //Get Employee model using empid number
  getEmployee(empid:number): Observable<Employee> {
    return this.http.get<Employee>(environment.employeeApi+"/"+empid)
  }
  //Get all Employee models in an array
    getAllEmployees(): Observable<Employee[]> {
      return this.http.get<Employee[]>(environment.employeeApi+"/")
    }
  //Get Manager model using empid number
  getManager(managerid:number): Observable<any> {
    return this.http.get<any>(environment.managerApi+"/"+managerid)
  }

  //Get Employee id using userid number
  getEmployeeId(userId:number): Observable<any>  {
    return this.http.get<any>("http://127.0.0.1:8000/karoapi/getEmployeeId/"+userId)
  }

  getAllFatigueRecords(): Observable<any> {
    return this.http.get<any>(environment.fatigueRecordApi+"/")
  }
  //Get Manager id using userid number
  getManagerId(userId:number): Observable<any>  {
    return this.http.get<any>("http://127.0.0.1:8000/karoapi/getManagerId/"+userId)
  }
 
  //Make server connection using empid number
  connectToServer(empid:number): Observable<any> {
    return this.http.get<any>(environment.connectionApi+"/"+empid)
  }
  //Mark specific notification as read using notification Id
  markNotificationAsRead(notificationid:number): Observable<Notification> {
    return this.http.post<any>(environment.markasreadApi+notificationid,{})
  }
  //Make a break request to the backend using empid number
  takeBreakRequest(empid:number): Observable<any> {
    return this.http.post<any>("http://127.0.0.1:8000/takeBreak/"+empid,{})
  }
  //connect to webcam to update time at desk
  connectWebcam(empid:number): Observable<EmployeeData> {
    return this.http.post<any>("http://127.0.0.1:8000/karoapi/connectWebcam/"+empid,{})
  }
  //Update blink
  updateBlink(empid:number,blinkAmt:number): Observable<Employee> {
    return this.http.patch<Employee>(environment.employeeApi+"/"+empid+"/", { blink : blinkAmt} )
  }
  //Send notification to a single employee
  sendEmployeeNotification(empid:number,text:string): Observable<any> {
    return this.http.post<any>("http://127.0.0.1:8000/karoapi/sendEmployeeNotification/",
    {
      "employee_id": empid,
      "text":text
    });
  }
  

  

}
