import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularMaterialModule } from './angular-material';
import { MatBadgeModule } from '@angular/material/badge';
import { NavComponent } from './nav/nav.component';
import { EmployeeOverviewDashboardComponent } from './dashboard/employee/employee-overview-dashboard/employee-overview-dashboard.component';
import { SystemRoutingModule } from './system-routing.module'
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength'
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatGridListModule} from '@angular/material/grid-list'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BackendService } from './service/backend-service.service';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthService, AuthInterceptor, AuthGuard } from './auth/auth.service';
import { ManagerOverviewDashboardComponent } from './dashboard/manager/manager-overview-dashboard/manager-overview-dashboard.component';
import { ResetPasswordComponent } from './login-page/reset-password/reset-password.component';
import { ResetPasswordRequestComponent } from './login-page/reset-password-request/reset-password-request.component';
import { ChartsModule } from 'ng2-charts';
import { ManagerEmployeelistComponent } from './manager-employeelist/manager-employeelist.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    EmployeeOverviewDashboardComponent,
    LoginPageComponent,
    ManagerOverviewDashboardComponent,
    ResetPasswordComponent,
    ResetPasswordRequestComponent,
    ManagerEmployeelistComponent,
    ProfilePageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SystemRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    MatGridListModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatBadgeModule,
    MatPasswordStrengthModule,
    ChartsModule
  ],
  providers: [
    { provide: BackendService },
    AuthService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
