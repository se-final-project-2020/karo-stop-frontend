import EmployeeData from "./employee-data";
import Notification from "./notification"
export default class NotificationBox {
    id: number;
    employee: EmployeeData
    notifications: Notification[];
}
