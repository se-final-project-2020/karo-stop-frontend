export default class BreakRequest {
    id:number;
    time:Date;
    employee:any;
    is_accepted:boolean;
    is_denied:boolean;
}
