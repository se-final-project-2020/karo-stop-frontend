import User from "./user.model"
export default class Employee {
    id: number;
    user: User;
    notifications: Notification[];
    time_of_last_break: any;
    fatigue_threshold: number;
    time_at_work: number;
    time_at_desk: number;
    on_break: boolean;
    blink: number;
    phone_number: String;
    dob: any; //Not sure of format, using any to be safe
    department: String;

}
