import NotificationBox from "./notification-box";
import Notification from "./notification";
import Employee from './employee';

export default class EmployeeData {
    id: number;
    employee: Employee;
    //notificationBox: NotificationBox;
    notifications: Notification[];
    lastBreakTime: number;
    fatigueThreshold: number;
    timeAtWork: number;
    timeAtDesk: number;
    webcamOn: boolean;
    faceDetected: boolean;
    onBreak: boolean;
    blink: number;

}
