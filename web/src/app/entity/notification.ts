import NotificationBox from "./notification-box";

export default class Notification {
    id: number;
    sender: String;
    text: String;
    time: String;
    is_read: boolean;
}
