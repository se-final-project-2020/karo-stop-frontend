import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';

import * as moment from 'moment';
import { interval } from 'rxjs';
import { BackendService } from 'src/app/service/backend-service.service';
@Component({
  selector: 'app-manager-overview-dashboard',
  templateUrl: './manager-overview-dashboard.component.html',
  styleUrls: ['./manager-overview-dashboard.component.css'],
  providers: [DatePipe]
})
export class ManagerOverviewDashboardComponent implements OnInit {

  //Line Chart
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [180, 480, 770, 90, 1000, 270, 400], label: 'Series C', yAxisID: 'y-axis-1' }
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: false,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;


  //Bar Chart 

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    },
    legend: {
      labels: {
        fontSize: 18,
        fontColor: 'white'
      }
    }

  };
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Fatigue Level' },
  ];
  //sTOLEN FROM INTERNET
  getDates = function (startDate, endDate) {
    var dates = [],
      currentDate = startDate,
      addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = addDays.call(currentDate, 1);
    }
    return dates;
  };
  lastBarChartUpdateTime;


  categorizeFatigueRecordForBarChart() {
    var timeLabels = []
    //Date from
    var from = new Date()
    var x = new Date(from)

    //Date to
    var to = new Date(from.setDate(from.getDate() - 6))
    to.setHours(0, 0, 0, 0)
    var timeDifference = this.getDates(to, x)

    timeDifference.forEach(function (date) {
      timeLabels.push(formatDate(date, 'dd/MM/yyyy', 'en'))
      console.log(formatDate(date, 'dd/MM/yyyy', 'en'));
    });
    var timeData = [0, 0, 0, 0, 0, 0, 0]
    var fatigueData = [0, 0, 0, 0, 0, 0, 0]
    var amountData = [0, 0, 0, 0, 0, 0, 0]
    this.http.get<any>("http://127.0.0.1:8000/fatigueRecords/").subscribe(fatigueRec => {
      var fatigueRecords = fatigueRec
      this.fatigueRecords = fatigueRec
      for (var i = 0; i < fatigueRecords.length; i++) {
        var fatRecordTime = new Date(fatigueRecords[i].time_of_record).getTime()
        for (var j = 0; j < timeDifference.length; j++) {
          if (isNaN(new Date(timeDifference[j + 1]).getTime())) {
            if (fatRecordTime >= new Date(timeDifference[j]).getTime() ) {
              fatigueData[j] += fatigueRecords[i].fatigue_threshold
              amountData[j]++
            }
          }
          else if (fatRecordTime >= new Date(timeDifference[j]).getTime() && fatRecordTime < new Date(timeDifference[j + 1]).getTime()) {
            fatigueData[j] += fatigueRecords[i].fatigue_threshold  
            amountData[j]++
          }
        }
      }
      for (var i = 0; i < amountData.length; i++) {
        if (amountData[i] != 0) {
          timeData[i] = +(fatigueData[i] / amountData[i]).toFixed(2);
        }
      }
      this.barChartLabels = timeLabels
      this.barChartData[0].data = timeData
      this.lastBarChartUpdateTime = new Date()
    });
  }




  constructor(private http: HttpClient, private backendService: BackendService) { }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  //pie
  public pieChartOptions: ChartOptions = {
    responsive: true,

    legend: {
      position: 'bottom',
      labels: {
        fontSize: 18,
        fontColor: 'white'
      }
    },

    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (ctx.dataset.data[ctx.dataIndex] > 0)
            return label + " : " + ctx.dataset.data[ctx.dataIndex];
          else
            return ""
        },
      }
    },
  };

  //Chart for display the employees with different fatigue levels

  public pieChartLabels: Label[] = [
    'Excellent',
    'Fine',
    'Slightly Fatigued',
    'Moderately Fatigued',
    'Highly Fatigued'
  ];
  public pieChartData: number[] = [300, 500, 100];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['#50b432', '#91ec0f', '#e0e15c', '#e5b604', '#ee3804'],
    },
  ];

  //Use the employeeData, and add the value of fatigue level count 
  //according to the fatigue level of employee
  categorizeFatigueLevelForPieChart() {
    var Excellent = 0
    var Fine = 0
    var SlightlyFatigued = 0
    var ModeratelyFatigued = 0
    var HighlyFatigued = 0
    var totalFatigue = 0
    var empList;
    this.http.get("http://127.0.0.1:8000/employees/").subscribe(employees => {
      empList = employees
      this.employees = employees
      for (var i = 0; i < empList.length; i++) {
        var fat = empList[i].fatigue_threshold
        if (fat < 1 && fat >= 0)
          Excellent++
        else if (fat < 2 && fat >= 1)
          Fine++
        else if (fat < 3 && fat >= 2)
          SlightlyFatigued++
        else if (fat < 4 && fat >= 3)
          ModeratelyFatigued++
        else if (fat >= 4)
          HighlyFatigued++

        totalFatigue += fat
        console.log(totalFatigue)
      }
      this.averageFatigue = totalFatigue / empList.length
      this.lastPieChartUpdateTime = new Date()

      this.pieChartData = [Excellent, Fine, SlightlyFatigued, ModeratelyFatigued, HighlyFatigued]

    });

  }
  //Indicate the average Fatigue of all the employees in the system
  averageFatigue: number = 0
  //Indicate the time the piechart was last refreshed
  lastPieChartUpdateTime: Date

  /*
  Fatigue color and value for ref
      color:#50b432 Excellent < 1 and >=0
      color:#91ec0f Fine < 2 and >=1
      color:#e0e15c Slightly Fatigued <3 and >=2
      color:#e5b604 Moderately Fatigued <4 and >=3
      color:#ee3804 Highly Fatigued >=4
  */
  employees: any = []

  //Array holding the fatigue records of employees
  fatigueRecords: Object = [];
  ngOnInit(): void {
    this.categorizeFatigueRecordForBarChart()
    this.categorizeFatigueLevelForPieChart()
    this.getAmountofEmployeeAtWork()
    this.autoUpdatecurrentTime
    this.autoUpdateAmountOfEmployeeAtWork
  }

  //Trigger on leaving the dashboard page
  ngOnDestroy(): void {

    this.autoUpdatecurrentTime.unsubscribe()
    this.autoUpdateAmountOfEmployeeAtWork.unsubscribe()
  }
  currentTime: number;
  totalEmployee;
  employeeAtWorkAmt
  //Indicate the time the amount of the employees at desk was last updated
  lastEmployeeAtWorkUpdateTime

  autoUpdatecurrentTime = interval(1000).subscribe(() => {
    this.currentTime = (new Date()).getTime() / 1000
  });
  autoUpdateAmountOfEmployeeAtWork = interval(10000).subscribe(() => { this.getAmountofEmployeeAtWork() });

  getAmountofEmployeeAtWork() {
    this.backendService.getAllEmployees().subscribe(data => {
      this.totalEmployee = data.length;
      this.employeeAtWorkAmt = data.filter(i => (i.time_at_work >= 0)).length
      this.lastEmployeeAtWorkUpdateTime = new Date()
    })
  }

  //Trigger on leaving page, refreshing page or closing browser
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler(event: Event) {
  }

  //Download the entire table of Fatigue Records as a csv file
  downloadFatigueRecordCSV(this_week) {
    this.http.post("http://127.0.0.1:8000/karoapi/getFatigueRecords/", { "this_week": this_week }, { responseType: 'arraybuffer' }
    ).subscribe(
      (res) => {
        let blob = new Blob([res], { type: 'text/csv' });
        if (this_week) {
          var date = new Date()
          var toDate = formatDate(date.toISOString(), 'yyyy-MM-dd', 'en')
          var date2 = new Date(date.setDate(date.getDate() - 6))
          var fromDate = formatDate(date2.toISOString(), 'yyyy-MM-dd', 'en')
          console.log(toDate, fromDate)
          FileSaver.saveAs(blob, "Fatigue Records " + fromDate + "_" + toDate + ".csv")
        }
        else
          FileSaver.saveAs(blob, "Fatigue Records.csv")
      }
    );
  }



}
