import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef} from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { interval } from 'rxjs';
import { getMatIconFailedToSanitizeUrlError } from '@angular/material/icon';
import { BackendService } from 'src/app/service/backend-service.service';
import { NavComponent } from 'src/app/nav/nav.component';
import * as faceapi from 'face-api.js';
import { AuthService } from 'src/app/auth/auth.service';
@Component({
  selector: 'app-employee-overview-dashboard',
  templateUrl: './employee-overview-dashboard.component.html',
  styleUrls: ['./employee-overview-dashboard.component.css']
})


export class EmployeeOverviewDashboardComponent implements OnInit {

  constructor(
    private backendService: BackendService,
    private http: HttpClient,
    private authService:AuthService) { }

  
  //Import to calculate Euclidean distance for eye aspect ratio
  distance = require('euclidean-distance')
  //Navigation bar from nav.component
  @ViewChild(NavComponent) 
  nav: NavComponent;
  //Webcam feed
  @ViewChild("video")
  video: ElementRef;
  //Facemark video overlay
  @ViewChild('facemarkCanvas')
  facemarkCanvas: ElementRef;

  employee = null

  //Status variables, might change how they work later on depending on time constraint and brainpower constraint
  currentTime: number;
  timeAtWork: number = 0;
  timeAtDesk: number = 0;
  FatigueThreshold: number = 0.00;
  FatigueDesc: string = "N/A"
  lastBreakTime: number = 0;

 

  //Default variables to calculate eye blinks
  EYE_AR_THRESH = 0.3
  EYE_AR_CONSEC_FRAMES = 3
    
  COUNTER = 0
  blink = 0
  //Resolution of the video
  videoRes
  
  //Amount of frames needed to for the face to be undetected 
  FACE_UNDETECTED_FRAMES_NEEDED = 90
  //Amount of frames without face detected
  FACE_UNDETECTED_FRAMES = 0
  
  //Blink detection refresh speed
  FPS = 30
  
  //Indicate if webcam is on
  webcamOn: boolean = false;  
  //Indicate if face is detected by the webcam
  faceDetected: boolean = false;
  //Indicate if face is losing tracking, but still consider face as detected within the timeframes
  losingFaceTracking: boolean = false;
  //Indicate if employee is on break
  onBreak: boolean = false;
  //Indicate the Employee user account
  user : any
  //Indicate Employee Id 
  empId

  
  //Calculate the eye aspect ratio
  private eye_aspect_ratio(eye) {
    var a = this.distance([eye[1]._x,eye[1]._y], [eye[5]._x,eye[5]._y])
    var b = this.distance([eye[2]._x,eye[2]._y], [eye[4]._x,eye[4]._y])
    var c = this.distance([eye[0]._x,eye[0]._y], [eye[3]._x,eye[3]._y])
    return (a + b) / (2.0 * c)
  }

  

  ngOnInit(): void {
    //Load facial landmark models on init
    faceapi.nets.faceLandmark68Net.loadFromUri('./assets/models'),
    //faceapi.nets.ssdMobilenetv1.loadFromUri('assets/models')
    faceapi.nets.tinyFaceDetector.loadFromUri('./assets/models')
    
    }


  ngAfterViewInit(): void {
    //Set up video resolution
    this.videoRes = {
      width: this.video.nativeElement.width, 
      height:this.video.nativeElement.height
    }
    //Assign user
    if (!this.user) {
      this.getUser().subscribe( data=> {
        this.user = data
        this.getEmployeeData()            
      })
    }
    else {
        this.getEmployeeData()
    }   
  }

  //Get Employee Data to display on the status panel
  private getEmployeeData() {
    //Fetch empId too if it is not defined
    if ( this.empId == undefined ) {
        //Fetch empId and then fetch EmployeeData
        this.backendService.getEmployeeId(this.user.id).subscribe( data=> {
            this.empId = data.employee_id;
            this.backendService.getEmployee(this.empId).subscribe( data => {
              this.employee =  data
              this.onBreak = data.on_break
              if (this.lastBreakTime == 0) { this.lastBreakTime = data.time_of_last_break }
              this.connectToServer()
            })
        })
    }
    else {
        //Fetch EmployeeData
        this.backendService.getEmployee(this.empId).subscribe( data => {
          this.employee =  data
          this.onBreak = data.on_break
          if (this.lastBreakTime == 0) { this.lastBreakTime = data.time_of_last_break }
          this.connectToServer()
        })
    }
  }


  getUser() {
    return this.http.get("http://127.0.0.1:8000/users/"+this.authService.getUser().user_id)
  }

  //Trigger on leaving the status panel page
  ngOnDestroy(): void {
    this.webcamOn = false;
    this.faceDetected = false;
    this.losingFaceTracking = false
    this.stopBlinkDetection()     
    this.a.unsubscribe()
    this.empId = undefined;
    
  }
  
  //Trigger on leaving page, refreshing page or closing browser
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler(event: Event) {
    this.webcamOn = false;
    this.stopBlinkDetection()   
    this.empId =  undefined;
  }


  //Start the webcam and blink detection process 
  startBlinkDetection() {
    if (!this.webcamOn) {
      if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            this.video.nativeElement.srcObject = stream;
            this.video.nativeElement.play();
            //Set webcam variable as on
            this.webcamOn = true; 
            //Trigger blink detection after 1 second
            setTimeout(
              ()=>{ 
               faceapi.matchDimensions(this.facemarkCanvas.nativeElement, {
                width: this.video.nativeElement.width, 
                height:this.video.nativeElement.height
              } )        
                this.faceApiDetectBlink()
              },1000 
            )           
        });   
      }
    }
  }

  //Stop the webcam and blink detection process 
  stopBlinkDetection() {
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      //navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
        this.facemarkCanvas.nativeElement.getContext('2d').clearRect(0, 0, this.facemarkCanvas.nativeElement.width, this.facemarkCanvas.nativeElement.height)  
        this.webcamOn = false;
        this.faceDetected = false;
        this.losingFaceTracking = false
        if (this.video.nativeElement.srcObject != null) {
          const tracks = this.video.nativeElement.srcObject.getTracks()
          if (tracks != null) {
            tracks.forEach(track => track.stop())
          }
      }
        this.video.nativeElement.srcObject = null;
        
      //});
    }
  }
   a = interval(1000).subscribe(() => {
    return this.backendService.connectToServer(this.empId).subscribe( data=> {
        this.currentTime = /*data.current*/(new Date()).getTime() / 1000
        this.timeAtWork = data.time_at_work
        this.FatigueThreshold = data.fatigue_threshold
        this.blink = data.blink
        this.timeAtDesk = data.time_at_desk
        if (this.lastBreakTime == 0) {
          console.log("adwad")
          this.lastBreakTime = data.time_of_last_break
        }
        this.checkBreakRequest()
        

        //Send data to server if face is detected
        if (this.faceDetected) {
          this.backendService.connectWebcam(this.empId).subscribe( data=> { 
        //Nothing here
        })   
      }


      })
  });
  //Connect to the backend server, refresh data every second
  connectToServer() {
  
    return this.a
  }

  //Detect blink from webcam using Faceapi.js
  async faceApiDetectBlink() {
    //If webcam is off, clean facial landmarks canvas and stop the function
    if (!this.webcamOn) {
      this.facemarkCanvas.nativeElement.getContext('2d').clearRect(0, 0, this.facemarkCanvas.nativeElement.width, this.facemarkCanvas.nativeElement.height)
      this.faceDetected = false
      this.losingFaceTracking = false
      return;
    }
    const detections = await faceapi.detectSingleFace(this.video.nativeElement, new faceapi.TinyFaceDetectorOptions({ scoreThreshold: 0.2 })).withFaceLandmarks();
    //If face can be detected from the webcam, mark as detected, detect face and eye blinks
    if (detections) {
    
      this.FACE_UNDETECTED_FRAMES = 0;
      this.losingFaceTracking = false
      this.faceDetected = true
      const resizedDetections = faceapi.resizeResults(detections, this.videoRes )
      this.facemarkCanvas.nativeElement.getContext('2d').clearRect(0, 0, this.facemarkCanvas.nativeElement.width, this.facemarkCanvas.nativeElement.height)
      faceapi.draw.drawFaceLandmarks(this.facemarkCanvas.nativeElement, resizedDetections)
      if (resizedDetections) {   
        const lefteyesLandmark = resizedDetections.landmarks.getLeftEye();
        const righteyesLandmark = resizedDetections.landmarks.getRightEye();
        var leftear = this.eye_aspect_ratio( lefteyesLandmark )
        var rightear = this.eye_aspect_ratio( righteyesLandmark )
        var ear = (leftear + rightear) / 2.0
        console.log(ear)
        // If Eye aspect ratio is less than the eye threshold, increment counter
        if (ear < this.EYE_AR_THRESH) {
            this.COUNTER += 1
        }
        else { 
            //If counter is more than eye threshold blink frames, count as blink and reset the counter to 0
            if (this.COUNTER >= this.EYE_AR_CONSEC_FRAMES) {
              this.blink += 1
              this.backendService.updateBlink(this.empId,this.blink).subscribe( data=> {
                //console.log(data.blink)
              })
            }
            this.COUNTER = 0
        }
      }
    }
    //Else if face can't be detected, increment face undetected frame
    else if (this.FACE_UNDETECTED_FRAMES <= this.FACE_UNDETECTED_FRAMES_NEEDED) {
          this.FACE_UNDETECTED_FRAMES+=1;
          if (this.FACE_UNDETECTED_FRAMES >= (this.FACE_UNDETECTED_FRAMES_NEEDED/3) ) {
            this.losingFaceTracking = true
          }
          //If face undetected frames is >= the needed frame, mark as undetected
          if (this.FACE_UNDETECTED_FRAMES >= this.FACE_UNDETECTED_FRAMES_NEEDED && this.faceDetected) {
            this.faceDetected = false;
          }
    }
    //Set delay and launch the same function again within delay
    var delay = (1000/this.FPS)
    setTimeout(() => this.faceApiDetectBlink(), delay)
  }


  //Mark unread notification as read
  markAsRead(notificationId) {
    this.backendService.markNotificationAsRead(notificationId).subscribe(data=> {
      console.log(data)
    })
  }

  takeBreakRequest() {
    {
        this.backendService.takeBreakRequest(this.empId).subscribe( data => {
          //If on break, stop the webcam too
          if (data.on_break) {
            this.onBreak = true
            this.lastBreakTime = data.time_of_last_break
            this.stopBlinkDetection()
          }
          else {
            this.onBreak = false
            this.breakPermitted = false
            //After taking the break, the breakRequest will be deleted
            this.http.delete("http://127.0.01:8000/breakRequests/"+this.break_request_id).subscribe(
              data=> {
                this.break_request_id = undefined;
              }
            )
          }
        });
    }
  }
  //Indicate the id of the break request
  break_request_id: number = undefined
  askBreakRequest() {
    {
      this.http.post("http://127.0.0.1:8000/breakRequests/", { "employee" : this.empId}).subscribe(
        data=> {
            this.breakRequestInReviewed = true
            this.backendService.sendEmployeeNotification(this.empId,"Your break request is being reviewed by the managers")
            .subscribe(data=>{

            })
           
        });
    }
  }
  

  //Indicate if Employee is permitted to take a break
  breakPermitted = false //Default to false
  //Indicate if the break request is being reviewed by managers
  breakRequestInReviewed = false //Default to false

  //Check if the breakRequest that the employee sends is still valid
  private checkBreakRequest() {
    this.http.get<any[]>("http://127.0.0.1:8000/breakRequests/").subscribe( data=> {
      console.log(data)
      //Find the breakRequest that belongs to the employee
      var latestBreakRequest = data.filter(t=> t.employee == this.empId)[0]
      if (latestBreakRequest) {
        this.break_request_id = latestBreakRequest.id
        this.breakRequestInReviewed = true
      }
      //If there is no break request
      if (latestBreakRequest == undefined) {
        this.breakPermitted = false
        this.breakRequestInReviewed = false
      }
      //If the break request is denied, delete the request and be done with it lmao
      else if ( (latestBreakRequest.is_denied && this.breakRequestInReviewed) ) {
        this.breakPermitted = false
        this.breakRequestInReviewed = false
        this.http.delete("http://127.0.01:8000/breakRequests/"+this.break_request_id).subscribe(
          data=> {
            this.break_request_id = undefined;
            this.backendService.sendEmployeeNotification(this.empId,"Your break request was denied by the managers")
            .subscribe(data=>{

            })
          }
        )

      }
      //If the break request is accepted, then permits break
      else if ( (latestBreakRequest.is_accepted) ) {
        
        
        if (this.breakRequestInReviewed && !this.breakPermitted) {
          this.backendService.sendEmployeeNotification(this.empId,"Your break request has been permitted by the managers")
          .subscribe(data=>{

          })
        } 
        this.breakPermitted = true
        this.breakRequestInReviewed = false
      }
      //If the break request is not yet answered 
      else if (!latestBreakRequest.is_accepted && !latestBreakRequest.is_denied) {
        //this.breakPermitted = false
        this.breakRequestInReviewed = true
      }
     
  
    })
  }

  /*
    I'm going to make a method to request for a break before actually making a break possible
  */
  

}


