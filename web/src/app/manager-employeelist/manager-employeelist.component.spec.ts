import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerEmployeelistComponent } from './manager-employeelist.component';

describe('ManagerEmployeelistComponent', () => {
  let component: ManagerEmployeelistComponent;
  let fixture: ComponentFixture<ManagerEmployeelistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerEmployeelistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerEmployeelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
