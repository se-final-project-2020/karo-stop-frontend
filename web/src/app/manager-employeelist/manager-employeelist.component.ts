import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../service/backend-service.service';
import { from, interval } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-employeelist',
  templateUrl: './manager-employeelist.component.html',
  styleUrls: ['./manager-employeelist.component.css']
})
export class ManagerEmployeelistComponent implements OnInit,AfterViewInit {
  //The columns that will be used to display 
  displayedColumns: string[] = ['name','position','fatigueLevel','view','edit'];


  //Real Data From BackEnd
  employeeData: any[] = [];

  //Indicate the time data was last updated
  lastEmployeeListUpdateTime: Date

  /*
    Karo Stop level and color for reference



  */
  
  constructor(
    private http:HttpClient,
    private backendService:BackendService,
    private router: Router
  ) { }

  //Trigger on leaving the page
  ngOnDestroy(): void {
    this.autoUpdateEmployeeList.unsubscribe()    
  }

  ngOnInit(): void {
    //Some code to get employee list from api
    this.updateEmployeeData()
    this.autoUpdateEmployeeList
  }
  //Update EmployeeData
  updateEmployeeData() {
    this.backendService.getAllEmployees().subscribe( employeesData=> {
      console.log(employeesData)
      this.employeeData = employeesData;
      console.log(this.employeeData)
      this.lastEmployeeListUpdateTime = new Date()
    })
  }

  autoUpdateEmployeeList = interval(5000).subscribe(() => { this.updateEmployeeData() });

  ngAfterViewInit() : void {
    
  }

  routeToEmployeeInfo(employeeId: number) {
    this.router.navigate(['manager/profile/',employeeId])
  }

}
