import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, interval } from 'rxjs';
import { BackendService } from '../service/backend-service.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {
  
  private routeSub: Subscription;
  employeeId: number;

  constructor(
    private http:HttpClient,
    private backendService:BackendService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params)
      console.log(params['id'])
      this.employeeId = params['id'];
      this.getEmployeeById(params['id']);
    });
    this.autoUpdate
    
  }

  ngOnDestory() {
    this.routeSub.unsubscribe();
    this.autoUpdate.unsubscribe()
  }

  employeeData: any;
  lastUpdateTime: Date;

  autoUpdate = interval(5000).subscribe(() => { this.getEmployeeById(this.employeeId) });
  getEmployeeById(empId: number) {
    return this.backendService.getEmployee(empId).subscribe( data => {
      console.log(data)
      this.employeeData = data;
      this.lastUpdateTime = new Date();
    });
  }

}
