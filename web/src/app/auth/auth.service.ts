import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { tap, shareReplay } from 'rxjs/operators';

import * as moment from 'moment';
//JWTPayload
interface JWTPayload {
  user_id: number;
  username: string;
  username_or_email: string;
  first_name: string;
  last_name: string;
  email: string;
  exp: number;
}
//AuthService
@Injectable()
export class AuthService { 

  private apiRoot = 'http://127.0.0.1:8000/auth/';

  constructor(private http: HttpClient) { }

   setSession(authResult) {
    const token = authResult.token;
    //Decode Token
    const payload = <JWTPayload> JSON.parse(atob(token.split('.')[1]))
    console.log(payload)
    const expiresAt = moment.unix(payload.exp);
    //Authentication token
    localStorage.setItem('token', authResult.token);
    //Authentication token exipire time
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  
  }

  get token(): string {
    return localStorage.getItem('token');
  }
  
  getUser() {
    const userToken = localStorage.getItem('token');
    if (userToken) {
      return JSON.parse(atob(userToken.split('.')[1]));
    } else {
      return null;
    }
  }

  login(username: string, password: string) {
     var username_or_email = username
    return this.http.post( this.apiRoot.concat('login/'), { /*username*/username_or_email, password } ).pipe(
      tap(response => this.setSession(response)),
      shareReplay(),
    );
  }

  signup(username: string, email: string, password1: string, password2: string) {
    // TODO: implement signup
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('expires_at');
  }
 
  refreshToken() {
    if (moment().isBetween(this.getExpiration().subtract(1, 'days'), this.getExpiration())) {
      return this.http.post(
        this.apiRoot.concat('refresh-token/'),
        { token: this.token }
      ).pipe(
        tap(response => this.setSession(response)),
        shareReplay(),
      ).subscribe();
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }
}
//AuthInterceptor
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const useJWT = true
    //JWT
    if (useJWT) {
      const token = localStorage.getItem('token');

        if (token) {
          const cloned = req.clone({
            headers: req.headers.set('Authorization', 'JWT '.concat(token))
          });

          return next.handle(cloned);
        } else {
          return next.handle(req);
        }
      }
    }
}
//AuthGuard
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) : Observable<boolean> | Promise<boolean> | boolean {
    const forStaff = next.data.for_staff;
    const userRole = this.authService.getUser().is_staff;
    //Check if is logged in and the required role for the page and user role match together
    if (this.authService.isLoggedIn() && (forStaff == userRole)) {
      this.authService.refreshToken();
      
      return true;
    } else {
      this.authService.logout();
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });

      return false;
    }
  }
}

